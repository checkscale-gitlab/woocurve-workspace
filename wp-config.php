<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );
@ini_set( 'display_errors', 1 );
define( 'SCRIPT_DEBUG', true );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'wpuser');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IwguRM#BZhYEHq^:GM&Ha>k5JU/{35v`o-%rPG-KW1Gm5_DV36okLvELTjxtn0p@');
define('SECURE_AUTH_KEY',  'bB@N4ivjVu8b!!1-2S%$/_N:;A$7}kJIQY5ID9|I^9U{FWwZF`0)C#uhrl1+;Vvr');
define('LOGGED_IN_KEY',    ',|+45;&G]Bd^={u1b^?uoiqR2etwp#Fz/HW]A=wmeD/KmsM@PNyp$P,O)^,oMI]a');
define('NONCE_KEY',        '!#Q{uKA.=JKt];hYtyNkb3E-L@_4/}BFj=3fe:},Y<^2ajYESD-5gq-58O1,jEnI');
define('AUTH_SALT',        '09Gr-~3|I|#sQ(?2Vg|K +=:{+KnyOT;dT85|*zb5[,+;jN?4v4A|=5O+9MX*Uk)');
define('SECURE_AUTH_SALT', 'r}i|tx}+6M(4ZP>nJ5[p6~Rf$}kygAk<c*gjxIF0R@?--sX!sg/.MaUz(+Pt$]y>');
define('LOGGED_IN_SALT',   'Cb=E*x<s7{Gk>(gp0N/8}+y(/QM+@rL;(H~bVnKjhKf1iTEVd^|,J?/k+PoO2y+Q');
define('NONCE_SALT',       '8`T9fY +v|b_u?Ql}|?Ae+:oN4|T<ei6s+=2PX-laH*;-ou+^`(;-1f]$aU/}>]k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
